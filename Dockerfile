# Use a Rust Docker image as the base
FROM rust:1.55 AS builder

# Set the working directory in the container
WORKDIR /usr/src/embed_lambda

# Copy the Cargo.lock and Cargo.toml files to the container
COPY embed/Cargo.toml embed/Cargo.lock ./

# Build the dependencies without the source code to cache dependencies
RUN mkdir src && \
    echo "fn main() {}" > src/main.rs && \
    cargo build --release

# Copy the source code to the container
COPY embed/src ./src

# Build the application
RUN cargo install --path .

# Use a minimal Alpine-based image for the final container
FROM alpine:latest

# Set the working directory in the container
WORKDIR /usr/src/embed_lambda

# Copy the binary from the builder stage to the final container
COPY --from=builder /usr/local/cargo/bin/embed_lambda .

# Expose any necessary ports
EXPOSE 8080

# Set the command to run the binary
CMD ["./embed_lambda"]
