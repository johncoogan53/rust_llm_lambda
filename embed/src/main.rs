use lambda_http::{lambda, Request, Response, Body, RequestExt};
use rust_bert::pipelines::common::ModelType;
use rust_bert::pipelines::text_encode::TextEncodePipeline;
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
struct InputPayload {
    tokens: Vec<String>,
}

#[derive(Debug, Deserialize, Serialize)]
struct OutputPayload {
    embedding: Vec<f64>,
}

fn handle_request(request: Request) -> Result<Response<Body>, lambda_http::Error> {
    // Parse the input JSON payload
    let input_payload: InputPayload = serde_json::from_slice(request.body().as_ref())?;

    // Initialize the sentence encoder model
    let model = TextEncodePipeline::new(ModelType::AllMiniLM_L12_v2)?;

    // Encode tokens
    let output = model.encode(&input_payload.tokens)?;

    // Prepare the output JSON payload
    let output_payload = OutputPayload { embedding: output };

    // Serialize the output JSON payload
    let body = serde_json::to_string(&output_payload)?;

    Ok(Response::builder()
        .status(200)
        .header("Content-Type", "application/json")
        .body(Body::Text(body))
        .expect("Failed to build response"))
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    lambda!(handle_request);

    Ok(())
}
